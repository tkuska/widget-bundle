<?php

namespace Tkuska\WidgetBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Tkuska\WidgetBundle\Entity\UploadedFile;
use Glifery\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
use Tkuska\WidgetBundle\Form\Transformers\UploadedFileTransformer;

class AjaxFileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fileInputOptions = [
            'required' => $options['required'],
            'error_bubbling' => true,
        ];

        $fileInputOptions = array_merge($fileInputOptions, $options['file_input_options']);
        $multiple = isset($options['file_input_options']['max_file_count']) && $options['file_input_options']['max_file_count'] > 1 ? true : false;

        $builder
            ->addViewTransformer(new UploadedFileTransformer([
                'file_input', 'uploaded_files',
            ]))
            ->add('file_input', FileType::class, $fileInputOptions)
            ->add('uploaded_files', EntityHiddenType::class, [
                'class' => UploadedFile::class,
                'multiple' => $multiple
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'file_input_options' => [],
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'tkuska_ajax_file';
    }
}

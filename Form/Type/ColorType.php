<?php

namespace Tkuska\WidgetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Description of ColorType.
 *
 * @author Tomasz Kuśka <tomasz.kuska@gmail.com>
 */
class ColorType extends AbstractType
{
    public function getParent(): string
    {
        return TextType::class;
    }
}

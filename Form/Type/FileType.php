<?php

namespace Tkuska\WidgetBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType as BaseFileType;
use Symfony\Component\Routing\RouterInterface;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Tkuska\WidgetBundle\Form\Listeners\UploadedFileSubscriber;
use Tkuska\WidgetBundle\Entity\UploadedFile;

class FileType extends BaseFileType
{
    /**
     * @var string
     */
    protected $dataClass = UploadedFile::class;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var UploadableManager
     */
    protected $uploadableManager;

    /**
     * @var ObjectManager
     */
    protected $entityManager;

    public function __construct(RouterInterface $router, UploadableManager $manager, EntityManagerInterface $entityManager, $dataClass = UploadedFile::class)
    {
        $this->dataClass = $dataClass;
        $this->router = $router;
        $this->uploadableManager = $manager;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->setAttribute('upload_route', $options['upload_route']);
        $builder->setAttribute('remove_route', $options['remove_route']);
        $builder->setAttribute('upload_async', $options['upload_async']);
        $builder->setAttribute('auto_replace', $options['auto_replace']);
        $builder->setAttribute('language', $options['language']);
        $builder->setAttribute('theme', $options['theme']);
        $builder->setAttribute('browse_class', $options['browse_class']);
        $builder->setAttribute('show_caption', $options['show_caption']);
        $builder->setAttribute('show_remove', $options['show_remove']);
        $builder->setAttribute('show_zoom', $options['show_zoom']);
        $builder->setAttribute('show_upload', $options['show_upload']);
        $builder->setAttribute('ajax', $options['ajax']);
        $builder->setAttribute('overwrite_initial', $options['overwrite_initial']);
        $builder->setAttribute('drop_zone_enabled', $options['drop_zone_enabled']);
        $builder->addEventSubscriber(new UploadedFileSubscriber($this->uploadableManager, $this->entityManager));
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['multiple'] = $options['multiple'] ? 'true' : 'false';
        $view->vars['max_file_count'] = $options['multiple'] ? $options['max_file_count'] : 1;
        $view->vars['upload_route'] = $this->router->generate($options['upload_route']);
        $view->vars['remove_route'] = $this->router->generate($options['remove_route']);
        $view->vars['upload_async'] = $options['upload_async'] ? 'true' : 'false';
        $view->vars['auto_replace'] = $options['auto_replace'] ? 'true' : 'false';
        $view->vars['language'] = $options['language'];
        $view->vars['theme'] = $options['theme'];
        $view->vars['browse_class'] = $options['browse_class'];
        $view->vars['show_caption'] = $options['show_caption'] ? 'true' : 'false';
        $view->vars['show_remove'] = $options['show_remove'] ? 'true' : 'false';
        $view->vars['show_zoom'] = $options['show_zoom'] ? 'true' : 'false';
        $view->vars['show_upload'] = $options['show_upload'] ? 'true' : 'false';
        $view->vars['ajax'] = $options['ajax'];
        $view->vars['overwrite_initial'] = $options['overwrite_initial'] ? 'true' : 'false';
        $view->vars['drop_zone_enabled'] = $options['drop_zone_enabled'] ? 'true' : 'false';
        //we are passing data always as array
        if (!$options['multiple'] && isset($view->vars['data'])) {
            $view->vars['data'] = [$view->vars['data']];
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'language' => 'en',
            'theme' => 'fas',
            'upload_async' => true,
            'upload_route' => 'tkuska_upload_file',
            'remove_route' => 'tkuska_remove_file',
            'overwrite_initial' => true,
            'ajax' => true,
            'browse_class' => null,
            'show_caption' => true,
            'show_remove' => true,
            'show_zoom' => true,
            'show_upload' => true,
            'max_file_count' => null,
            'auto_replace' => false,
            'drop_zone_enabled' => true,
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'tkuska_file';
    }
}

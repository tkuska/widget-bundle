<?php

namespace Tkuska\WidgetBundle\Form\Listeners;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Tkuska\WidgetBundle\Entity\UploadedFile;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;

class UploadedFileSubscriber implements EventSubscriberInterface
{
    /**
     * @var UploadableManager
     */
    private $uploadableManager;

    /**
     * @var ObjectManager
     */
    private $entityManager;

    public function __construct(UploadableManager $uploadableManager, EntityManagerInterface $entityManager)
    {
        $this->uploadableManager = $uploadableManager;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::SUBMIT => 'onSubmit',
        ];
    }

    public function onSubmit(FormEvent $event)
    {
        $data = $event->getData();
        if (!$data) {
            return;
        }
        if(is_array($data)){
            $resp = new ArrayCollection();
            foreach ($data as $item) {
                $file = $this->persistFile($item);
                $resp->add($file);
            }
        }
        else {
            $resp = $this->persistFile($data);
        }
        $event->setData($resp);

    }

    private function persistFile($data): UploadedFile
    {
        $file = new UploadedFile();
        $file->setOriginalFilename($data->getClientOriginalName());
        $file->setOriginalExtension($data->getClientOriginalExtension());
        $this->uploadableManager->markEntityToUpload($file, $data);
        $this->entityManager->persist($file);
        $this->entityManager->flush();

        return $file;
    }
}

<?php

namespace Tkuska\WidgetBundle\Form\Transformers;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Description of UploadedFileTransformer.
 *
 * @author Tomasz Kuśka <tomasz.kuska@gmail.com>
 */
class UploadedFileTransformer implements DataTransformerInterface
{
    /**
     * Duplicates the given value through the array.
     *
     * @param mixed $value The value
     *
     * @return array The array
     */
    public function transform($value)
    {
        $result = [
            'file_input' => $value,
            'uploaded_files' => $value,
        ];

        return $result;
    }

    /**
     * Extracts the duplicated value from an array.
     *
     * @param array $array
     *
     * @throws TransformationFailedException if the given value is not an array or
     *                                       if the given array can not be transformed
     *
     * @return mixed The value
     */
    public function reverseTransform($array)
    {
        if (!is_array($array)) {
            throw new TransformationFailedException('Expected an array.');
        }

        //jeżeli nie został dograny nowy plik (przez HTML) to zwracamy to co dotychczas było dograne
        if (!$array['file_input']){
            //@TODO: do wnikliwej analizy
            if(is_array($array['file_input']) && is_null($array['uploaded_files'])){
                return [];
            }
            return $array['uploaded_files'];
        }

        return $array['file_input'];
    }
}

<?php

namespace Tkuska\WidgetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tkuska\WidgetBundle\Entity\UploadedFile;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Tkuska\WidgetBundle\Events\DownloadFileEvent;
use Tkuska\WidgetBundle\Events\RemoveFileEvent;
use Tkuska\WidgetBundle\Events\UploadFileEvent;

/**
 * Uloaded file controller.
 */
#[Route(path: '/fileinput')]
class UploadController extends AbstractController
{
    /**
     * Pobieranie pracowników firmy.
     *
     */
    #[Route(path: '/upload', name: 'tkuska_upload_file', methods: ['PUT', 'POST'])]
    public function uploadAction(Request $request, EventDispatcherInterface $dispatcher)
    {
        $form = $this->createForm(\AppBundle\Form\ReportTypeType::class);
        $form->handleRequest($request);

        $file = $form->getData()->getFile();

        $dispatcher->dispatch(new UploadFileEvent($file));
        $resp = [
            'initialPreview' => [],
            'initialPreviewConfig' => [
                'caption' => '',
                'width' => '120px',
                'url' => $this->generateUrl('tkuska_remove_file'),
                'key' => '',
                'extra' => ['id' => $file->getId()],
            ],
            'append' => true,
        ];

        return new JsonResponse($resp);
    }

    /**
     * Pobieranie pracowników firmy.
     *
     */
    #[Route(path: '/remove', name: 'tkuska_remove_file', methods: ['PUT', 'POST'])]
    public function removeAction(Request $request, EventDispatcherInterface $dispatcher)
    {
        $id = $request->get('key');

        $em = $this->getDoctrine()->getManager();
        $uploadedFile = $em->getRepository(UploadedFile::class)
            ->find($id);

        $dispatcher->dispatch(new RemoveFileEvent($uploadedFile));
        $em->remove($uploadedFile);
        $em->flush();

        return new JsonResponse('OK');
    }

    /**
     * @param UploadedFile $file
     *
     * @return BinaryFileResponse
     */
    #[Route(path: '/{id}/download"', name: 'tkuska_download_file', methods: ['GET'])]
    public function downloadAction(UploadedFile $file, EventDispatcherInterface $dispatcher)
    {
        $dispatcher->dispatch(new DownloadFileEvent($file));
        $response = new BinaryFileResponse($file->getPath());

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file->getOriginalFilename());

        return $response;
    }
}

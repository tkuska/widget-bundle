<?php

namespace Tkuska\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Description of File.
 *
 * @author Tomasz Kuśka <tomasz.kuska@gmail.com>
 * @Gedmo\Uploadable(allowOverwrite=true, appendNumber=true, filenameGenerator="SHA1"))
 * @ORM\Entity
 * @ORM\Table(name="tk_uploaded_files")
 */
class UploadedFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $originalFilename;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $originalExtension;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Gedmo\UploadableFilePath
     */
    private $path;

    /**
     * @ORM\Column(name="filesize", type="decimal", nullable=true)
     * @Gedmo\UploadableFileSize
     */
    private $size;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Gedmo\UploadableFileMimeType
     */
    private $mimeType;

    public function __toString()
    {
        return $this->originalFilename;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalFilename.
     *
     * @param string $originalFilename
     *
     * @return UploadedFile
     */
    public function setOriginalFilename($originalFilename)
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return UploadedFile
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Set size.
     *
     * @param string $size
     *
     * @return UploadedFile
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Set mimeType.
     *
     * @param string $mimeType
     *
     * @return UploadedFile
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get originalFilename.
     *
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get size.
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get mimeType.
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set originalExtension.
     *
     * @param string $originalExtension
     *
     * @return UploadedFile
     */
    public function setOriginalExtension($originalExtension)
    {
        $this->originalExtension = $originalExtension;

        return $this;
    }

    /**
     * Get originalExtension.
     *
     * @return string
     */
    public function getOriginalExtension()
    {
        return $this->originalExtension;
    }
}

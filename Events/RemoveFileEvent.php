<?php
namespace Tkuska\WidgetBundle\Events;


use Symfony\Contracts\EventDispatcher\Event;
use Tkuska\WidgetBundle\Entity\UploadedFile;

class RemoveFileEvent extends Event
{
    private $file;

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    public function getFile(){
        return $this->file;
    }

}
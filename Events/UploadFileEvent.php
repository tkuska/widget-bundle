<?php


namespace Tkuska\WidgetBundle\Events;


use Tkuska\WidgetBundle\Entity\UploadedFile;

class UploadFileEvent
{
    private $file;

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    public function getFile(){
        return $this->file;
    }
}
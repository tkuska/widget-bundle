$(document).ready(function () {
    moment.locale("pl");
    $('.input-group.date').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: moment.locale("pl")
    });
    $('.input-group.time').datetimepicker({
        format: 'LT',
        locale: moment.locale("pl")
    });
    $('.input-group.datetime').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD LT',
        locale: moment.locale("pl")
    });
});